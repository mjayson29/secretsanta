<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class santamodel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function getUserByHash($hash){
		// $sql = "SELECT * FROM participants 
		// 	WHERE p_hash = '".$hash."'";
		$this -> db -> SELECT('*')
			-> WHERE('p_hash', $hash)
			-> FROM('participants');

		$sql = $this -> db -> get();
		return $sql->result();
	}

	function updateTable($table,$data,$id){
		$this -> db -> WHERE($id);
		$this -> db -> UPDATE($table, $data);
	}

	function getAllUsers(){
		$this -> db -> SELECT('*')
			-> FROM('participants');
		$query = $this -> db -> get();

		return $query -> result();
	}
}
