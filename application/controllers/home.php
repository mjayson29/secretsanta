<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function  __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Manila");
		$this->load->model('santamodel','santaModel');
	}

	public function index(){
		$data['user'] = $this->santaModel->getAllUsers();
		$this->load->view('register',$data);
	}

	function register($hash=NULL){
		
		if($_POST){
			$hash1 = $this->input->post('hash');
			$user = $this->santaModel->getUserByHash($hash1);
			if(count($user) > 0){
				$wish = $this->input->post('wish');
				$wish1 = $this->input->post('wish1');
				$wish2 = $this->input->post('wish2');
				$note = $this->input->post('note');

				foreach($user as $row){
					$data = array(
						'p_wishes'=> $wish.'||'.$wish1.'||'.$wish2,
						'p_note'=> $note,
						'p_active'=> 1);
					$table = "participants";
					$id = "p_hash = '".$row->p_hash."'";
					$this->santaModel->updateTable($table,$data,$id);

					$subject = "Secret Santa :)";
					$message = "<style> .email_format{ font-family: 'arial';  font-size: 16px; } </style>
							<div class='email_format'>
							Hello ".$row->p_codename.",<br><br> Thank you for completing the registration. We will email you your monito/monita once all other participants are done in registration.<br>
						<br><br><br>Happy Holidays!
						</div>";
					$this->send_email($row->p_email, $message, $subject);
				}
				echo "SUCCESS";
			}else{
				echo "Failed";
			}
			
		}else{
			$user = $this->santaModel->getUserByHash($hash);
			$data['user'] = $user;
			$this->load->view('register',$data);
		}
	}

	function broadcast(){
		$users = $this->santaModel->getAllUsers();
		foreach($users as $user){
			$hash = hash('adler32', date('Y-m-d H:i:s').$user->p_email);
			$data = array('p_hash' => $hash);
			$table = "participants";
			$id = "p_id = ".$user->p_id;
			$this->santaModel->updateTable($table,$data,$id);

			$subject = "Secret Santa :)";
			$message = "<style> .email_format{ font-family: 'arial';  font-size: 16px; } </style>
							<div class='email_format'>
							Hello ".$user->p_codename.",<br><br> Kindly access the below URL to formally join to Mandalay Exchange gift.<br>
				<a href='".base_url()."home/register/".$hash."'>Register</a><br><br><br>Happy Holidays!
				</div>";
			$this->send_email($user->p_email, $message, $subject);
		}
	}

	function send_email($email, $message, $subject){
		$this->load->library('email');
		$this->email->initialize(array(
				'protocol' => 'smtp',
				'smtp_host' => '192.168.7.83',
				'smtp_user' => '',
				'smtp_pass' => '',
				'smtp_port' => 25,
				'crlf' => "\r\n",
				'newline' => "\r\n",
				'mailtype' => 'html'
		));
		$this->email->from('noreply@mandalay.com.ph', "Secret Santa");
		$this->email->bcc($email);
		$this->email->subject($subject);
		$this->email->message($message);
		if($this->email->send()){
			$status = "Success";
		}else{
			$status = "Failed";
		}
		$now = date('T Y-m-d H:i:s');
		$file = APPPATH.'../application/logs/mailogs.txt';
		if(!file_exists($file)){
			$file = fopen(APPPATH.'../application/logs/mailogs.txt', "w");
		}
		$logs = file_get_contents($file);
		if(is_array($email)){
			foreach ($email as $key => $value) {
				$logs .= $now."\t".$subject."\t".$message."\t".$value."\t".$status."\r\n";
			}
		}else{
			$logs .= $now."\t".$subject."\t".$message."\t".$email."\t".$status."\r\n";
		}
		
		file_put_contents($file, $logs,  LOCK_EX);
	}
}
